package com.techuniversity;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Users {
    private static JSONObject data = null;

    public static void cargarUsers() throws FileNotFoundException {
        try(InputStream resourceAsStream = Users.class.getClassLoader().getResourceAsStream( "users.JSON" )){
            InputStreamReader isr = new InputStreamReader(resourceAsStream);
            BufferedReader br = new BufferedReader(isr);
            String line;
            String str = "";

            while ((line = br.readLine()) != null){
                str += line;
            }

            System.out.println(str);
        }catch( Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    public static JSONObject getUser(String id) throws JSONException, FileNotFoundException{
        //Si no se carga data se llama a la función para no procesar un null
        if(data == null) cargarUsers();

        JSONObject setUser = null;
        //Se obtiene el arrar de objetos del JSON
        JSONArray users = data.getJSONArray("users");

        //Se itera el array para procesar cada objeto
        for ( int i=0; i< users.length(); i++){
            //Se asigna el objeto
            JSONObject user = users.getJSONObject(i);

            //Se obtiene el valor del ID
            String idUser = user.getString("userId");
            //Si es igual al ID buscado se devuelve el objeto y se rompe el ciclo
            if (idUser.equals(id)){
                setUser = user;
                break;
            }
        }
        return setUser;
    }
}
